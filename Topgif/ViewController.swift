//
//  ViewController.swift
//  Topgif
//
//  Created by Tim Broddin on 18/09/15.
//  Copyright © 2015 Tim Broddin. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController, NSURLConnectionDelegate {

    @IBOutlet weak var image: UIImageView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        showGif();
        
        let swipeRecognizer = UISwipeGestureRecognizer(target: self, action:"next:")
        swipeRecognizer.direction = .Left
        self.view.addGestureRecognizer(swipeRecognizer)


    }
    
    func next(sender: UITapGestureRecognizer? = nil) {
        showGif();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func load_image(urlString:String)
    {
        print(urlString);
        
        let imgURL: NSURL = NSURL(string: urlString)!
        let request: NSURLRequest = NSURLRequest(URL: imgURL)
        NSURLConnection.sendAsynchronousRequest(
            request, queue: NSOperationQueue.mainQueue(),
            completionHandler: {(response: NSURLResponse?,data: NSData?,error: NSError?) -> Void in
                if error == nil {
                    let testImage = UIImage.animatedImageWithAnimatedGIFData(data)
                    self.image.animationImages = testImage.images
                    self.image.animationDuration = testImage.duration
                    self.image.animationRepeatCount = -1
                    self.image.image = testImage.images?.last
                    self.image.startAnimating()
                    
                    self.delay(20) {
                        self.showGif()
                    }
                }
        })
        
    }
    
    func showGif() {
        print("Showing gif");
        let json = JSON(url:"http://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC")
        print(json);
        load_image(json["data"]["image_original_url"].asString!)
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }


}

